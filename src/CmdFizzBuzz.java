/**
 * David's Java Terminal FizzBuzz Program
 * A CLI application written in Java that does the classic FizzBuzz operation.
 * 
 * @author David Lucadou
 * Copyright (C) 2016 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class CmdFizzBuzz {
	public static void cmdFizzBuzz() {
		for(int i = 1; i <= 100; i++) {
			if(i % 3 == 0 && i % 5 == 0)
				System.out.print("FizzBuzz ");
			else if(i % 3 == 0 && i % 5 != 0)
				System.out.print("Fizz ");
			else if(i % 3 != 0 && i % 5 == 0)
				System.out.print("Buzz ");
			else if(i % 3 != 0 && i % 5 != 0)
				System.out.print(i + " ");
		}
	}
}
