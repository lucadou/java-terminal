/**
 * David's Java Terminal Credits Program
 * A CLI application written in Java that displays the StackOverflow,
 * etc. links that made writing this program so much easier.
 * 
 * @author David Lucadou
 * Copyright (C) 2015-2016 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.ArrayList;

public class CmdCredits {
	public static void cmdCredits(ArrayList<String> arrayCmd) {
		int totalArgs = 0;
		for(int i = 0; i < arrayCmd.size(); i++) {
			if(arrayCmd.get(i) != null) {
				totalArgs++;
			}
		}
		for(int i = 0; i < totalArgs; i++) {
			if(arrayCmd.get(i).equalsIgnoreCase("Credits")) {
				listCmdCredits();
			} else if(arrayCmd.get(i).equalsIgnoreCase("CommandParser")) {
				listCommandParserCredits();
			} else if(arrayCmd.get(i).equalsIgnoreCase("JohnCena")) {
				listJohnCenaCredits();
			} else if(arrayCmd.get(i).equalsIgnoreCase("LennyFace")) {
				listLennyFaceCredits();
			} else if(arrayCmd.get(i).equalsIgnoreCase("Terminal") || arrayCmd.get(i).equalsIgnoreCase("TerminalLaunch")) {
				listTerminalLaunchCredits();
			} else if(arrayCmd.get(i).equalsIgnoreCase("TicTacToe")) {
				listTicTacToeCredits();
			}
		}
	}
	
	private static void listCmdCredits() {
		System.out.println("Achievement Get: Being Meta!");
		System.out.println("Credits for the command \"credits\" (class CmdCredits.java):");
		System.out.println("1. https://stackoverflow.com/questions/4441099/how-do-you-count-the-elements-of-an-array-in-java\nFor the for loop used in this program to count the number of elements in an array.");
	}
	
	private static void listCommandParserCredits() {
		System.out.println("Credits for the class CommandParser.java:");
		System.out.println("1. https://stackoverflow.com/questions/1610757/pass-array-to-method-java\nUsed this to finally implement arrays in all classes instead of passing entire Strings with spaces.");
	}
	
	private static void listTerminalLaunchCredits() {
		System.out.println("Credits for the class TerminalLaunch.java (no notes added due to the # of credits):");
		System.out.println("1. http://examples.javacodegeeks.com/core-java/util/arraylist/arraylist-in-java-example-how-to-use-arraylist/");
		System.out.println("2. https://stackoverflow.com/questions/14207005/warning-arraylist-is-a-raw-type-references-to-generic-type-arrayliste-should");
		System.out.println("3. https://stackoverflow.com/questions/1610757/pass-array-to-method-java");
		System.out.println("4. https://stackoverflow.com/questions/10268464/null-pointer-access-the-variable-data-can-only-be-null-at-this-location");
		System.out.println("5. https://stackoverflow.com/questions/5919143/is-it-safe-not-to-close-a-java-scanner-provided-i-close-the-underlying-readable\nI'll make an exception for #5-7 just because this was such a huge bug to solve.");
		System.out.println("6. https://stackoverflow.com/questions/12874791/at-java-util-scanner-throwforunknown-source-error");
		System.out.println("7. https://stackoverflow.com/questions/13042008/java-util-nosuchelementexception-scanner-reading-user-input");
		System.out.println("If you ever have a problem with NoSuchElementException errors, remove [ScannerName].close(); statements for all classes without a main class (or are not the launch point of the program.");
		System.out.println("The reason for this is that once ANY Scanner is closed, System.in is closed for ALL classes and you CANNOT reinstate System.in.");
	}
	
	private static void listTicTacToeCredits() {
		System.out.println("Credits for the command \"TicTacToe\" (class CmdTicTacToe.java):");
		System.out.println("1. http://stackoverflow.com/questions/5585779/converting-string-to-int-in-java\nVery handy in converting numbers in Strings to integers, as charAt() returns...ASCII values. Yeah, I don't know, either.");
		System.out.println("2. http://www.utf8-chartable.de/unicode-utf8-table.pl\nUsed this to rewrite my drawBoard() method to use UTF-8 so it would properly display.");
	}
	
	private static void listJohnCenaCredits() {
		System.out.println("Credits for the command \"JohnCena\" (class CmdJohnCena.java):");
		System.out.println("1. http://picascii.com/ - Beautiful ASCII art.");
		System.out.println("2. Notepad++ ToolBucket Plugin - Multi-line find and replace saved tons of time.");
		System.out.println("3. The Z-Morning Zoo - For inspiring all of this.");
		System.out.println("4. John Cena - For being Champ.");
	}
	
	private static void listLennyFaceCredits() {
		System.out.println("Credits for the command \"LennyFace\" (class CmdLennyFace.java):");
		System.out.println("1. http://www.alexdantas.net/lenny/ - ASCII art.");
	}
}
