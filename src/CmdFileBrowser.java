/**
 * David's Java Terminal File Browser Application
 * A CLI application written in Java that acts as a file manager.
 * 
 * @author David Lucadou
 * Copyright (C) 2015-2016 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;

//TODO: beta test all move combinations, implement args, and help finish files.
//http://docs.oracle.com/javase/8/docs/api/java/io/File.html
//http://docs.oracle.com/javase/8/docs/api/java/io/File.html#list--
//http://stackoverflow.com/questions/5125242/java-list-only-subdirectories-from-a-directory-not-files
//http://www.cs.carleton.edu/faculty/dmusican/cs117s03/iocheat.html
//http://stackoverflow.com/questions/5015833/how-to-store-file-list-from-console-to-directory-using-java
//http://stackoverflow.com/questions/7494478/jfilechooser-from-a-command-line-program-and-popping-up-underneath-all-windows
//http://stackoverflow.com/questions/15512200/get-path-object-from-file
//http://stackoverflow.com/questions/34363892/copy-files-from-one-directory-to-another-without-replacement
//replace all instances of if/else for boolean inversion with <variable> = !<variable>;

public class CmdFileBrowser {
	private static int sizeOfArray;
	//private String curDir, primaryInput, userInput;
	private static Path curDir;
	private static String curDirStr, primaryInput, userInput, osType, fileSeparator, notFileSeparator;
	private static Scanner kb = new Scanner(System.in);
	
	/**
	 * It's <code>main</code>, there's nothing special about it.
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		cmdFileBrowser();
	}
	
	
	/**
	 * This is the starting class for the class CmdFileBrowser.<br>
	 * It prompts the user for a command, and sends it to the
	 * method FBLCmdParser, which decides what to do from there.<br>
	 * When "exit" or "close" is entered, it exits to the main
	 * TerminalLaunch class.
	 * @throws IOException
	 */
	public static void cmdFileBrowser() throws IOException { //ArrayList<String> ArrayCmd)
		curDir = getPath(System.getProperty("user.home"));
		curDirStr = FileUtils.getUserDirectoryPath();
		if (System.getProperty("os.name").indexOf("Windows") != -1) {
			osType = "Win32";
		} else {
			osType = "Linux/Unix";
		}
		fileSeparator = System.getProperty("file.separator");
		if (fileSeparator.equals("/")) {
			notFileSeparator = "\\";
		} else if (fileSeparator.equals("\\")) {
			notFileSeparator = "/";
		} else {
			System.out.println("Error: variable fileSeparator was given an unexpected value. Please contact the author of this program for assistance.\nDebugging information:\nos.name=" + osType + "\nfileSeparator=" + fileSeparator + " notFileSeparator=" + notFileSeparator); //notFileSeparator should be null, but more information = better for debugging.
		}
		
		do {
			if (osType.equalsIgnoreCase("Win32")) {
				System.out.print("user@java " + curDirStr + "$ ");
			} else if (osType.equalsIgnoreCase("Linux/Unix")) {
				System.out.print("user@java: " + curDirStr + "$ ");
			} else {
				System.out.print("user@java | " + curDirStr + "$");
			}
			userInput = kb.nextLine();
			
			ArrayList<String> inputArray = stringToArrayList(userInput);
			
			fblCmdParser(inputArray);
		} while (!userInput.equalsIgnoreCase("exit") &&  !userInput.equalsIgnoreCase("close"));
	}
	
	/**
	 * Changes the current working directory.<br>
	 * Prints "That is not a valid directory" if it is not a valid directory.
	 * @param dirToUse The working directory to change to.
	 */
	private static void fblChangeDir(String dirToUse) {
		dirToUse = parsePath(dirToUse);
		System.out.println("[DEBUG]:\ncurDir=" + curDir.toString() + "\ncurDirStr=" + curDirStr);
		/*if (dirToUse.equalsIgnoreCase(".")) { // "." does nothing, since it means current directory, does not change directories.
			return;
		} else if (dirToUse.equalsIgnoreCase("..")) {
			System.out.println("curDir=" + curDir.toString() + " curDirStr=" + curDirStr);
			curDir = getParent(curDirStr);
			curDirStr = curDir.toString();
			System.out.println("curDir=" + curDir.toString() + " curDirStr=" + curDirStr);
		} else*/ if (fblDirOrFile(getPath(dirToUse)) == 0) {
			curDir = getPath(dirToUse);
			curDirStr = dirToUse;
		} else {
			
			if (dirToUse.contains(("." + fileSeparator))) { //.\someSubDirectory
				String newDir = curDir.toString() + dirToUse.substring(2); // Starting at 2 means the./ or\ is removed.
				File tmp = new File(newDir);
				if(tmp.isDirectory()) {
					curDir = getPath(tmp);
					curDirStr = curDir.toString();
				} else {
					System.out.println("Invalid directory: " + newDir);
				}
			} else {
				System.out.println("Invalid directory: " + dirToUse);
			}
		}
	}
	
	/**
	 * Method for the copy command when no argument is given.
	 * @param target - File/folder to be copied.
	 * @param destination - Where target is to be copied to.
	 * @throws IOException
	 */
	public static void fblCopy(String target, String destination) throws IOException {
		fblCopy(null, target, destination);
	}
	
	/**
	 * Copies a file or folder to the specified directory.
	 * @param arg0 - String, parameter to change the behavior of the command.
	 * @param target - File/folder to be copied.
	 * @param destination - Where target is to be copied to.
	 * @throws IOException
	 */
	public static void fblCopy(String arg0, String target, String destination) throws IOException {
		target = parsePath(target);
		destination = parsePath(destination);
		//Used to determine how the command will operate.
		int typeO = fblDirOrFile(getPath(target));
		int typeN = fblDirOrFile(getPath(destination));
		
		//Debug:
		System.out.println("[DEBUG]:\ntarget=" + target + "\ndestination=" + destination + "\ntypeO=" + typeO + " typeN=" + typeN);
		
		//target = parsePath(target);
		//destination = parsePath(destination);
		
		/* This block of code removes any \'s from the String target to get
		 * the name of the file or folder being copied and sets that for the
		 * String fileName, for use in various places throughout this method.
		 */
		String fileName = "";
		if (target.lastIndexOf(fileSeparator) == target.length() - 1) { //target ends with "\", must be a folder.
			fileName = target.substring(target.lastIndexOf(fileSeparator, target.length() - 2) + 1);//, target.lastIndexOf(fileSeparator));
		} else { //target does not end with "\", either a file or folder.
			fileName = target.substring(target.lastIndexOf(fileSeparator) + 1);
		}
		
		String finalDestination = destination;
		if (destination.lastIndexOf(fileSeparator) == destination.length() - 1 && //destination ends with "\"
				destination.lastIndexOf(fileSeparator, destination.length() - 2) != -1) { //&& this is not the last "\" in destination.
			if (destination.indexOf(fileName, destination.lastIndexOf(fileSeparator, destination.length() - 2)) == -1) { //destination does not contain fileName, will be added to finalDestination for proper typeN results.
				finalDestination = destination + fileName;
			} else { //destination does contain fileName, and it needs to be removed for the copy operation to work properly.
				finalDestination = destination;
				destination = destination.substring(0, destination.indexOf(fileName, destination.lastIndexOf(fileSeparator, destination.length() - 2)));
			}
		} else if (destination.lastIndexOf(fileSeparator) != destination.length() - 1) { //destination does NOT end with "\".
			if (destination.indexOf(fileName, destination.lastIndexOf(fileSeparator)) != -1) { //destination does contain fileName, needs to be removed for the copy operation to work properly. 
				destination = destination.substring(0, destination.indexOf(fileName, destination.lastIndexOf(fileSeparator)));
			} else { //destination does contain fileName, will be added to finalDestination for proper typeN result.				
				finalDestination = destination + fileSeparator + fileName;
			}
		}
		
		typeN = fblDirOrFile(getPath(finalDestination));		
		File targetF = new File(target);
		File destinationF = new File(destination);
		//System.out.println("DEBUG: typeO=" + typeO + " typeN=" + typeN);
		
		if (typeO == -1) {
			System.out.println("Error: target does not exist.");
		} else if (typeO == 0 && typeN == -1) { // Old is a folder, new does not exist.
			//A new folder is created. The contents of targetF are copied, then deleted.
			FileUtils.copyDirectory(targetF, destinationF);
			//moveDirectory requires the destination directory not exist (typeN == -1).
			//moveDirectoryToDirectory requires the destination directory to already exist.
			//Since typeN == -1, the destination directory does not already exist.
		} else if (typeO == 0 && typeN == 0) { // Old is a folder, new is a folder of the same name.
			try {
				Files.copy(getPath(targetF), getPath(destinationF));
			} catch (FileAlreadyExistsException ex) {
				System.out.println("One or more files in the destination directory already exist. Overwrite? (Y/N)");
				String response = kb.nextLine();
				if (inputYesOrNo(response)) {
					FileUtils.copyDirectoryToDirectory(targetF, destinationF);
					/*
					 * I am not going to use the Java Files class for this. Apache makes it MUCH easier.
					 * (The following has been copied and pasted from the Git-commit message I made because I don't want to retype it.)
					 * Basically, when using the built in Java Files class,
					 * copying a directory to a directory which already
					 * exists will cause a FileAlreadyExistsException. Using
					 * StandardCopyOptions.REPLACE_EXISTING only works if you
					 * have a for loop which parses each folder to append
					 * foo.txt to /path/. Each target essentially becomes
					 * /from/foo.txt, /to/foo.txt rather than /from/foo.txt, /to.
					 * Because I do not want to write that code out, I am using
					 * the FileUtils.copyDirectoryToDirectory() method when the
					 * user says they want to overwrite the contents of the
					 * directory. The for loop in the else statement was from
					 * StackOverflow, source cited after the loop.
					 */
				} else {
					System.out.println("Continue copying the rest of the files in this folder? (Y/N)");
					response = kb.nextLine();
					if (inputYesOrNo(response)) {
						for (Path f : getPath(target)) {
							String targetPath = targetF + System.getProperty(fileSeparator) + f.getFileName();
							File targetFile = new File(targetPath);
							if (!targetFile.exists()) {
								Files.copy(f, Paths.get(targetFile.toString()).resolve(f.getFileName()));
							}
						} //Credit for this block of code goes to: http://stackoverflow.com/questions/34363892/copy-files-from-one-directory-to-another-without-replacement
					}
				}
			}
		} else if (typeO == 0 && typeN == 1) { // Old is a folder, new is a file.
			try {
				Files.copy(getPath(targetF), getPath(destinationF), (CopyOption)null);
			} catch (NullPointerException ex) {
				System.out.println("A file of the same name already exists in the destination directory.");
				System.out.println("Please rename the item you want to copy and try again.");
			} catch (FileNotFoundException ex) {
				//System.out.println("FileNotFoundException ex has been successfully caught.");
				System.out.println("Error: Access to the resource " + fileName + " has been disallowed.");
				System.out.println("Please try again as an administrator/root.\nIf you already are running this program under an elevated security context, this file likely requires system level permissions to access.");
			}
		} else if (typeO == 0 && typeN == 2) { // Old is a folder, new is both a folder and a file.
			System.out.println("Both a file and a folder in the destination directory share the same name.");
			System.out.println("This is technically impossible, regardless of whether you are using a Win32 or UNIX compatible system.");
			System.out.println("Please consult outside help. This should not be happening.");
		} else if (typeO == 1 && typeN == -1) { // Old is a file, new does not exist.
			try {
				FileUtils.copyFileToDirectory(targetF, destinationF, true);
			} catch (FileNotFoundException ex) {
				//System.out.println("FileNotFoundException ex has been successfully caught.");
				System.out.println("Error: Access to the resource " + fileName + " has been disallowed.");
				System.out.println("Please try again as an administrator/root.\nIf you already are running this program under an elevated security context, this file likely requires system level permissions to access.");
			}
		} else if (typeO == 1 && typeN == 0) { // Old is a file, new is a folder.
			try {
				Files.copy(getPath(targetF), getPath(destinationF), (CopyOption)null);
			} catch (NullPointerException ex) {
				String destinationWithFile = finalDestination + fileSeparator + fileName;
				System.out.println("The file which you want to copy, " + fileName + ", is being copied into a directory which contains a folder of the same name.");
				System.out.print("Do you wish to copy " + fileName + " to the directory " + destinationWithFile + "? (Y/N) ");
				String input = kb.nextLine();
				if (inputYesOrNo(input)) {
					fblCopy(target, destinationWithFile);
					/*
					 * By calling the function recursively, it prevents NullPointerException errors
					 * from occurring because the file may already exist inside the target directory,
					 * and I do not have to re-implement the typeO=1 and typeN=1 else-if logic again.
					 */
				} else {
					System.out.println("You cannot have a file and a folder of the same name in the same directory.");
					System.out.println("Please rename the file you wish to copy and try again.");
				}
			} catch (FileNotFoundException ex) {
				//System.out.println("FileNotFoundException ex has been successfully caught.");
				System.out.println("Error: Access to the resource " + fileName + " has been disallowed.");
				System.out.println("Please try again as an administrator/root.\nIf you already are running this program under an elevated security context, this file likely requires system level permissions to access.");
			}
		} else if (typeO == 1 && typeN == 1) { // Old is a file, new is a file (name conflict).
			System.out.print("This file already exists in the destination directory. Do you want to overwrite it? (Y/N) ");
			String response = kb.nextLine();
			if (inputYesOrNo(response)) {
				FileUtils.copyFileToDirectory(targetF, destinationF);
			} else {
				System.out.println("File not copied.");
			}
		} else if (typeO == 1 && typeN == 2) { // Old is a file, new is both a file and a folder (name conflict).
			System.out.println("Both a file and a folder in the destination directory share the same name.");
			System.out.println("This is technically impossible, regardless of whether you are using a Win32 or UNIX compatible system.");
			System.out.println("Please consult outside help. This should not be happening.");
		} else if (typeO == 2) {
			System.out.println("Both a file and a folder in the source directory share the same name.");
			System.out.println("This is technically impossible, regardless of whether you are using a Win32 or UNIX compatible system.");
			System.out.println("Please consult outside help. This should not be happening.");
		}
	}
	
	/**
	 * Lists the files and folders present in the current directory.
	 * @param dirToUse - String of the directory to list.
	 * @param curDir2 - Honestly I don't know why this is here...I should probably remove it.
	 * @throws IOException
	 */
	private static void fblList(String dirToUse, Path curDir2) throws IOException {
		/*String testStr = "";
		if (!(Files.isDirectory(getPath(dirToUse)))) {
			if (curDirStr.substring(dirToUse.length() - 1).equals(fileSeparator)) {
				testStr = curDirStr + dirToUse;
				if (Files.isDirectory(getPath(testStr))) {
					dirToUse = testStr;
				}
			} else {
				testStr = curDirStr + fileSeparator + dirToUse;
				if (Files.isDirectory(getPath(testStr))) {
					dirToUse = testStr;
				}
			}
		}*/
		
		dirToUse = parsePath(dirToUse);
		
		try {
			//System.out.println("[DEBUG] dirToUse=" + dirToUse);
		} catch (NullPointerException ex) {
			System.out.println("[DEBUG] dirToUse=null in method fblList()!");
			return;
		}
		//System.out.println(dirToUse);
		
		curDir2 = getPath(dirToUse);
		
		File[] fblDirsList = new File(dirToUse).listFiles(File::isDirectory);
		File[] fblFilesList = new File(dirToUse).listFiles(File::isFile);
		
		if (!(fblDirsList == null) && !dirToUse.equalsIgnoreCase(".") && !dirToUse.equalsIgnoreCase("..") && fblDirOrFile(getPath(dirToUse)) == 0) {
			System.out.println("    Directory of " + dirToUse);
			System.out.println("<DIR>	    .      (Current Directory)");
			System.out.println("<DIR>	    ..     (Parent Directory)");
			for (int i = 0; i < fblDirsList.length; i++) {
				String curDirPerms = fblGetPerms(fblDirsList[i]);
				String folderToPrint = fblDirsList[i].toString().substring(fblDirsList[i].toString().lastIndexOf(fileSeparator) + 1);
				System.out.println("<DIR>	" + curDirPerms + " " + folderToPrint);//fblDirsList[i]);
			}
			for (int i = 0; i < fblFilesList.length; i++) {
				String curFilePerms = fblGetPerms(fblFilesList[i]);
				String fileToPrint = fblFilesList[i].toString().substring(fblFilesList[i].toString().lastIndexOf(fileSeparator) + 1);
				System.out.println("	" + curFilePerms + " " + fileToPrint);
			}
		} else if(!(fblDirsList == null) && dirToUse.equalsIgnoreCase(".") && fblDirOrFile(curDir2) == 0) {
			// "." indicates current directory, so curDir will work better than also bringing in curDir, etc.
			fblDirsList = curDir2.toFile().listFiles(File::isDirectory);
			fblFilesList = curDir2.toFile().listFiles(File::isFile);

			System.out.println("    Directory of " + dirToUse);
			System.out.println("<DIR>	    .      (Current Directory)");
			System.out.println("<DIR>	    ..     (Parent Directory)");
			for (int i = 1; i < fblDirsList.length; i++) {
				String curDirPerms = fblGetPerms(fblDirsList[i]);
				System.out.println("<DIR>	" + curDirPerms + " " + fblDirsList[i]);
			}
			for (int i = 0; i < fblFilesList.length; i++) {
				String curFilePerms = fblGetPerms(fblFilesList[i]);
				System.out.println("	" + curFilePerms + " " + fblFilesList[i]);
			}
		} else if(!(fblDirsList == null) && dirToUse.equalsIgnoreCase("..")) {
			dirToUse = getParent(curDirStr).toString();//getParent(curDir2.toString()).toString();
			fblDirsList = new File(dirToUse).listFiles(File::isDirectory);
			fblFilesList = new File(dirToUse).listFiles(File::isFile);

			System.out.println("    Directory of " + dirToUse);
			System.out.println("<DIR>	    .      (Current Directory)");
			System.out.println("<DIR>	    ..     (Parent Directory)");
			for (int i = 1; i < fblDirsList.length; i++) {
				String curDirPerms = fblGetPerms(fblDirsList[i]);
				System.out.println("<DIR>	" + curDirPerms + " " + fblDirsList[i]);
			}
			for (int i = 0; i < fblFilesList.length; i++) {
				String curFilePerms = fblGetPerms(fblFilesList[i]);
				System.out.println("	" + curFilePerms + " " + fblFilesList[i]);
			}
		} else {
			System.out.println("Error: Invalid directory name. Please enter a valid directory.");
		}
	}
	
	/**
	 * Method for the move command when no argument is given.
	 * @param oldP - File/folder to be moved.
	 * @param newP - Destination directory of oldP.
	 * @throws IOException
	 */
	private static void fblMove(String oldP, String newP) throws IOException {
		fblMove(null, oldP, newP);
	}
	
	/**
	 * Moves a file or folder to the specified directory.
	 * @param arg0 - String, parameter to change the behavior of the command.
	 * @param oldP - File/folder to be moved.
	 * @param newP - Destination directory of oldP.
	 * @throws IOException
	 */
	private static void fblMove(String arg0, String oldP, String newP) throws IOException {
		int typeO = fblDirOrFile(getPath(oldP));
		int typeN = fblDirOrFile(getPath(newP));
		
		oldP = parsePath(oldP);
		newP = parsePath(newP);
		
		File oldPF = new File(oldP);
		File newPF = new File(newP);
		
		if (typeO == 0 && typeN == -1) { // Old is a folder, new does not exist.
			// A new folder is created. The contents of oldPF are copied, then deleted.
			FileUtils.moveDirectory(oldPF, newPF);
			// moveDirectory requires the destination directory not exist (typeN == -1).
			// moveDirectoryToDirectory requires the destination directory to already exist.
			// Since typeN == -1, the destination directory does not already exist.
		} else if (typeO == 0 && typeN == 0) { // Old is a folder, new is a folder of the same name.
			//WILL THIS OVERWRITE OTHER FILES? FOLDERS? OR CRASH? ETC! NEED TO TEST!
			//This means it copies folder .\Test1\Abc into .\Test2\Abc. Need a prompt to overwrite/merge.
			FileUtils.moveDirectoryToDirectory(oldPF, newPF, false); //add prompt if they want to create new dir if not exists already
		} else if (typeO == 0 && typeN == 1) { // Old is a folder, new is a file.
			FileUtils.moveDirectory(oldPF, newPF);
		} else if (typeO == 0 && typeN == 2) { // Old is a folder, new is both a folder and a file.
			System.out.println("Both a file and a folder in the target directory share the same name.");
			System.out.println("Please rename the item you want to move and try again.");
		} else if (typeO == 1 && typeN == -1) { // Old is a file, new does not exist.
			FileUtils.moveFileToDirectory(oldPF, newPF, true);
		} else if (typeO == 1 && typeN == 0) { // Old is a file, new is a folder.
			try {
				FileUtils.moveFileToDirectory(oldPF, newPF, false);
			} catch (FileExistsException ex) {
				System.out.println("A folder of the same name already exists. Overwrite? (Y/N)"); //add y/n/replace all files that already exist, add new ones that dont already exist
				String response = kb.nextLine();
				if (inputYesOrNo(response)) {
					FileUtils.moveDirectoryToDirectory(oldPF, newPF, true);
					/*
					 * Reused from fblCopy() method.
					 */
				} else {
					System.out.println("Continue moving the rest of the files in this folder? (Y/N)");
					response = kb.nextLine();
					if (inputYesOrNo(response)) {
						for (Path f : getPath(oldP)) {
							String targetPath = newP + System.getProperty(fileSeparator) + f.getFileName();
							File targetFile = new File(targetPath);
							if (!targetFile.exists()) {
								Files.move(f, Paths.get(targetFile.toString()).resolve(f.getFileName()));
							}
						} //Credit for this block of code goes to: http://stackoverflow.com/questions/34363892/copy-files-from-one-directory-to-another-without-replacement
					}
				}
			}
		} else if (typeO == 1 && typeN == 1) { // Old is a file, new is a file (name conflict).
			System.out.print("This file already exists in the target directory. Do you want to overwrite it? (Y/N) ");
			String response = kb.nextLine();
			if (inputYesOrNo(response)) {
				Files.move(getPath(oldP), getPath(newP), StandardCopyOption.REPLACE_EXISTING);
			} else {
				System.out.println("File not moved.");
			}
		} else if (typeO == 1 && typeN == 2) { // Old is a file, new is both a file and a folder (name conflict).
			
		} else if (typeO == 2) {
			System.out.println("Both a file and a folder in this directory share the same name.");
			System.out.println("Please rename the item you want to move to move it.");
		}
	}
	
	private static void fblRunProgram(String path) throws IOException {
		fblRunProgram (path, new ArrayList<String>()); //Sends an empty ArrayList if no parameters given.
	}
	
	private static void fblRunProgram(String path, ArrayList<String> params) throws IOException {
		//stuff to use:
		//http://docs.oracle.com/javase/7/docs/api/java/lang/ProcessBuilder.html
		//http://stackoverflow.com/questions/13991007/execute-external-program-in-java
		File workingDir = new File(curDirStr); //Will be used to set the working directory the program is launched in to the directory specified by curDirStr.
		ProcessBuilder proc;
		String pathToProgram = "";
		
		pathToProgram = parsePath(path); //Need to test it to see if it works with C:\program.exe vs program when working dir = C:\.
		
		if (params.size() > 0) { //No parameters given, i.e. "foo".
			proc = new ProcessBuilder(pathToProgram);
			proc.directory(workingDir); //Sets the current working directory to workingDir (aka curDirStr as type File).
			proc.start(); //Starts the program.
		} else { //Parameters given that need to be formatted, i.e. "foo -bar".
			String parameters = "";
			for (int i = 0; i < params.size(); i++) {
				if (i != params.size() - 1) { //Prevents a trailing space after the last element/parameter.
					parameters += params.get(i) + " ";
				} else {
					parameters += params.get(i);
				}
			}
			proc = new ProcessBuilder(pathToProgram, parameters);
			proc.directory(workingDir); //Sets the current working directory to workingDir (aka curDirStr as type File).
			proc.start(); //Starts the program.
		}
	}
	
	/**
	 * Parses the commands given to it.
	 * @param inputArray - ArrayList(String) to parse.
	 * @throws IOException
	 */
	private static void fblCmdParser(ArrayList<String> inputArray) throws IOException {
		primaryInput = inputArray.get(0);
		
		if (primaryInput.equalsIgnoreCase("cd")) {
			fblChangeDir(inputArray.get(1));
		} else if (primaryInput.equalsIgnoreCase("close") || primaryInput.equalsIgnoreCase("exit")) {
			userInput = "close";
			/* I'm doing this because the do-while loop exits when userInput == "close" or "exit".
			 * If the user enters "close exit", "exit close", "close 123", "exit 529", etc. it will
			 * not exit as intended. This detects if the first word is "close" or "exit" and modify
			 * userInput to be "close", so it properly exits when the do-while loop checks to see if
			 * it should exit or not.
			 */
			System.out.println("Goodbye!");
		} else if (primaryInput.equalsIgnoreCase("cp") || primaryInput.equalsIgnoreCase("copy")) {
			if (inputArray.size() == 4) { //Includes an argument at index 1.
				fblCopy(inputArray.get(1), inputArray.get(2), inputArray.get(3));
			} else if(inputArray.size() == 3) {
				fblCopy(inputArray.get(1), inputArray.get(2));
			} else {
				System.out.println("Error: Improper command syntax.");
				System.out.println("Type \"help " + primaryInput + "\" for proper syntax."); //Using primaryInput so I don't need to know if "cp" or "copy" is used.
			}
		} else if (primaryInput.equalsIgnoreCase("help") || primaryInput.equalsIgnoreCase("halp")) {
			fblHelp(inputArray);
		} else if (primaryInput.equalsIgnoreCase("ls") || primaryInput.equalsIgnoreCase("dir")) {
			if (inputArray.size() > 1) {
				fblList(inputArray.get(1), curDir);
			} else {
				fblList(curDir.toString(), curDir);
			}
		} else if (primaryInput.equalsIgnoreCase("mv") || primaryInput.equals("ren")) {
			if (inputArray.size() == 4) { //Includes an argument at index 1.
				fblMove(inputArray.get(1), inputArray.get(2), inputArray.get(3));
			} else if(inputArray.size() == 3) {
				fblMove(inputArray.get(1), inputArray.get(2));
			} else {
				System.out.println("Error: Improper command syntax.");
				System.out.println("Type \"help " + primaryInput + "\" for proper syntax."); //Using primaryInput so I don't need to know if "mv" or "move" is used.
			}
		} else if (primaryInput.equalsIgnoreCase("run") || primaryInput.equalsIgnoreCase("exec") || primaryInput.equalsIgnoreCase("execute") || primaryInput.equalsIgnoreCase("start")) {
			if (inputArray.size() == 2) {	
				fblRunProgram (inputArray.get(2));
			} else if (inputArray.size() > 2){
				ArrayList<String> parameters = new ArrayList<String>();
				for (int i = 3; i < inputArray.size(); i++) {
					parameters.add(inputArray.get(i));
				}
			} else {
				System.out.println("Error: Unknown error. inputArray.size()=" + inputArray.size() + "\ninputArray=\n");
				for (int i = 0; i < inputArray.size(); i++) {
					System.out.println(inputArray.get(i));
				}
				//Not sure if this would ever be needed, but I'll leave it in anyway for debugging purposes.
			}
		} else {
			System.out.println("Error: Invalid command. Please enter a valid command, or type \"help\" for a list of commands.");
		}
	}
	
	/**
	 * Takes a File and returns whether or not the current user is allowed
	 * to read from, write to, or execute the selected file or directory.
	 * @param filePath - The file or folder to read permissions for.
	 * @return A String representation of the permissions for the selected
	 * directory (Read, Write, and Execute), with a space left for each
	 * permission the user lacks.
	 */
	private static String fblGetPerms(File filePath) {
		String canExecute = "";
		String canRead = "";
		String canWrite = "";
		if (filePath.canRead()) {
			canRead = "R";
		} else {
			canRead = " ";
		}
		
		if (filePath.canWrite()) {
			canWrite = "W";
		} else {
			canWrite = " ";
		}
		
		if (filePath.canExecute()) {
			canExecute = "X";
		} else {
			canExecute = " ";
		}
		return canRead + canWrite + canExecute;
	}
	
	/**
	 * Prints a help menu for the command(s) specified. Multiple commands can be
	 * specified in the same command. For example, the input "help cd mv" would
	 * result in the help menus for both the cd and mv commands being printed.
	 * @param inputArray - An ArrayList to parse to display the relevant help menu.
	 */
	private static void fblHelp(ArrayList<String> inputArray) {
		if (sizeOfArray > 1) {
			for (int i = 1; i < sizeOfArray; i++) {
				if (((String) inputArray.get(i)).equalsIgnoreCase("help")) {
					System.out.println("Achievement Get: Being Meta!");
					System.out.println("Usage: help [command]");
					System.out.println("No options available.");
					System.out.println("");
				} else if (((String) inputArray.get(i)).equalsIgnoreCase("cd")) {
					System.out.println("Changes the current working directory.");
					System.out.println("Usage: cd [directory]");
					System.out.println("[directory]		Changes to the specified directory.");
					System.out.println("");
				} else if (((String) inputArray.get(i)).equalsIgnoreCase("close")) {
					System.out.println("Exits this program and returns to the previous one.");
					System.out.println("Usage: close");
					System.out.println("See also: exit");
				} else if (((String) inputArray.get(i)).equalsIgnoreCase("dir")) {
					System.out.println("Lists the contents of the specified directory.");
					System.out.println("If no directory is specified, the current working directory is used.");
					System.out.println("Usage: dir [directory]");
					System.out.println("[directory]		Indicates a directory to use other than the current directory.");
					System.out.println("See also: ls");
					System.out.println("");
				} else if(((String) inputArray.get(i)).equalsIgnoreCase("exit")) {
					System.out.println("Exits this program and returns to the previous one.");
					System.out.println("Usage: exit");
					System.out.println("See also: close");
				} else if(((String) inputArray.get(i)).equalsIgnoreCase("ls")) {
					System.out.println("Lists the contents of the specified directory.");
					System.out.println("If no directory is specified, the current working directory is used.");
					System.out.println("Usage: ls [directory]");
					System.out.println("[directory]		Indicates a directory to use other than the current directory.");
					System.out.println("See also: dir");
					System.out.println("");
				} else if(((String) inputArray.get(i)).equalsIgnoreCase("mv")) {
					System.out.println("This method is not yet complete. Still in the debugging phase. Fill out when complete.");
				} else if(((String) inputArray.get(i)).equalsIgnoreCase("move")) {
					System.out.println("This method is not yet complete. Still in the debugging phase. Fill out when complete.");
				}
			}
		} else {
			System.out.println("cd, close, dir, exit, ls, mv\nnote to self: finish this.");
		}
	}
	
	/**
	 * Tells you if the specified path (as a String) is a valid directory, file, or neither.
	 * 
	 * @param curDir2 The path to test.
	 * @return		Status on whether the path leads to a file, directory, or is invalid.<br>
	 * 				-1 = Invalid path, does not lead to a file or directory.<br>
	 * 				0 = Valid directory.<br>
	 * 				1 = Valid file.<br>
	 * 				2 = The specified path exists as both a valid file and a valid directory.<br>
	 */
	private static int fblDirOrFile(Path curDir2) {
		int output;
		File test = curDir2.toFile();
		if (test.isDirectory()) { // Uses File class.
			output = 0; // Is a valid directory.
		} else if (test.isFile()) { // Uses File class.
			output = 1; // Is a valid file.
		} else {
			output = -1; // Is not a valid directory or file.
		}
		if (test.isDirectory() && test.isFile()) { // Done last just in case both a file AND a folder exist with the same name.
			output = 2; // Name is used by both a valid directory AND a valid file.
		}
		return output;
	}
	
	/**
	 * Takes a String and returns an ArrayList of each word with spaces removed.
	 * @param userInput String
	 * @return An ArrayList(String) of the words in the given String.
	 */
	private static ArrayList<String> tmpoldstringToArrayList(String userInput) {
		ArrayList<String> inputArray = new ArrayList<String>();
		int numOfSpaces = 0, numOfWords = 0, numOfQuotes = 0, curSpacePos = 0, nextSpacePos = 0, curQuotePos = 0, nextQuotePos = 0, quoteIndex = 0;
		String primaryInput = "";
		
		if (userInput.contains(" ")) {
			primaryInput = userInput.substring(0, userInput.indexOf(" "));
		} else {
			primaryInput = userInput;
		}
		
		if (isCommand(primaryInput)) {
			if (!(userInput.contains("\""))) {
				inputArray.clear();
				//		curSpacePos = 0;
				//		nextSpacePos = 0;
				//		numOfSpaces = 0;
				//		numOfWords = 0;
				ArrayList<Integer> spacesIndex = new ArrayList<Integer>();
				ArrayList<Integer> quotesIndex = new ArrayList<Integer>();
				spacesIndex.add(0);
				quotesIndex.add(0);
				for (int i = 0; i < userInput.length(); i++) {
					if (userInput.charAt(i) == ' ') {
						numOfSpaces++;
						spacesIndex.add(i);
					}
				}
				for (int i = 0; i < userInput.length(); i++) {
					if (userInput.charAt(i) == '\"') {
						numOfQuotes++;
						quotesIndex.add(i);
					}
				}
				spacesIndex.add(-1); //Added for if-statements at the end of the for-loop below.
				quotesIndex.add(-1);
				numOfWords = numOfSpaces + 1;
				sizeOfArray = numOfWords;
				if (numOfWords > 1 && numOfSpaces >= 1) {
					if (quotesIndex.size() > 2) { //I added 0 and -1 to the beginning and end, so it is only 2 if there are no quotes.
						//Actually, you know what? I just need to redo this entirely. Base it around quotes first, THEN spaces.
						for (int i = 0; i < numOfWords; i++) {
							curSpacePos = spacesIndex.get(i);
							nextSpacePos = spacesIndex.get(i + 1);
							if (i == 0 && curSpacePos >= 0) {
								inputArray.add(i, userInput.substring(0, nextSpacePos));
							} else if (nextSpacePos >= curSpacePos) { //There is another word to add to the ArrayList.
								inputArray.add(i, userInput.substring(curSpacePos + 1, nextSpacePos));
							} else if (nextSpacePos != curSpacePos && nextSpacePos == -1) { //This is the last word.
								inputArray.add(i, userInput.substring(curSpacePos + 1));
							} else if (nextSpacePos == curSpacePos && curSpacePos != -1) {
								inputArray.add(i, userInput.substring(curSpacePos + 1));
							} else if (curSpacePos == -1) { //This should not occur; this is to catch unexpected errors.
								System.out.println(
										"An unexpected error has occured in the stringToArrayList method's for loop in CmdFileBrowser.java. Please contact the author of this program for assistance.\nError: curSpacePos == -1. This error was successfully caught and recovered from.");
								inputArray.add(i, userInput.substring(0));
							} else {
								System.out.println(
										"An unexpected error has occured in the stringToArrayList method's for loop in CmdFileBrowser.java. Please contact the author of this program for assistance.\nError: else statement reached. This error was successfully caught.");
							}
						}
					}

				} else if (numOfWords == 1 && numOfSpaces == 0) {
					inputArray.add(userInput);
				} else {
					System.out.println(
							"An unexpected error has occured in the stringToArrayList method. Please contact the author of this program for assistance.\nError: The else statement was reached, meaning there is an invalid number of words or spaces. numOfWords="
									+ numOfWords + "numOfSpaces=" + numOfSpaces);
				}
			} else {
				ArrayList<Integer> spacesIndex = new ArrayList<Integer>();
				ArrayList<Integer> quotesIndex = new ArrayList<Integer>();
				int spacesOutsideQuotes = 0;
				boolean quoteOpen = false;
				for (int i = 0; i < userInput.length(); i++) {
					if (userInput.charAt(i) == '\"') {
						quoteOpen = !quoteOpen;
						quotesIndex.add(i);
					numOfQuotes++;
					} else {
						if (!quoteOpen && userInput.charAt(i) == ' ') {
							spacesOutsideQuotes++;
							spacesIndex.add(i);
							numOfSpaces++;
						}
					}
				}
				numOfWords = numOfSpaces - 1;
				
				if (true) {
					
				}
			} 
		}
		return inputArray;
	}
	
	private static ArrayList<String> stringToArrayList(String userInput) {
		ArrayList<String> inputArray = new ArrayList<String>();
		
		if (userInput.indexOf(" ") == -1) { //&& userInput.indexOf("\"") == -1) {
			inputArray.add(userInput);
		} else {
			inputArray.add(userInput.substring(0, userInput.indexOf(" "))); //Adds the first word.
			boolean inQuote = false;
			String curWord = "";
			for (int i = userInput.indexOf(" "); i < userInput.length(); i++) {
				if (userInput.charAt(i) == '\"') {
					inQuote = !inQuote;
					if (!inQuote) { //inQuote is now false; it was previously true, so this is the end of the parameter.
						if (!(curWord.equals(""))) {
							inputArray.add(curWord);
						}
						curWord = "";
					}
				} else {
					if (userInput.charAt(i) == ' ') {
						if (inQuote) { //If inQuote == true, the space is part of the current parameter.
							curWord += Character.toString(userInput.charAt(i));
						} else {
							if (!(curWord.equals(""))) {
								inputArray.add(curWord);
							}
							curWord = "";
						}
					} else {
						curWord += Character.toString(userInput.charAt(i));
						if (i == userInput.length() - 1) {
							if (!(curWord.equals(""))) {
								inputArray.add(curWord);
							}
							curWord = "";
						}
					}
				}
			}
		}
		
		System.out.println("[DEBUG] inputArray:");
		for (int i = 0; i < inputArray.size(); i++) {
			System.out.println("Index " + i + ": " + inputArray.get(i));
		}
		
		return inputArray;
	}
	
	private static ArrayList<String> oldstringToArrayList(String userInput) {
		ArrayList<String> inputArray = new ArrayList<String>();
		int numOfSpaces = 0, numOfWords = 0, numOfQuotes = 0, curSpacePos = 0, nextSpacePos = 0, curQuotePos = 0, nextQuotePos = 0;
		
		inputArray.clear();
//		curSpacePos = 0;
//		nextSpacePos = 0;
//		numOfSpaces = 0;
//		numOfWords = 0;
		ArrayList<Integer> spacesIndex = new ArrayList<Integer>();
		ArrayList<Integer> quotesIndex = new ArrayList<Integer>();
		spacesIndex.add(0);
		quotesIndex.add(0);
		
		for (int i = 0; i < userInput.length(); i++) {
			if (userInput.charAt(i) == ' ') {
				numOfSpaces++;
				spacesIndex.add(i);
			} else if (userInput.charAt(i) == '\"') {
				numOfQuotes++;
				quotesIndex.add(i);
			}
		}
		spacesIndex.add(-1); //Added for if-statements at the end of the for-loop below.
		quotesIndex.add(-1);
		numOfWords = numOfSpaces + 1;
		sizeOfArray = numOfWords;
		if (numOfWords > 1 && numOfSpaces >= 1) {
			for (int i = 0; i < numOfWords; i++) {
				curSpacePos = spacesIndex.get(i);
				nextSpacePos = spacesIndex.get(i + 1);
				if (i == 0 && curSpacePos >= 0) {
					inputArray.add(i, userInput.substring(0, nextSpacePos));
				} else if (nextSpacePos >= curSpacePos) { //There is another word to add to the ArrayList.
					inputArray.add(i, userInput.substring(curSpacePos + 1, nextSpacePos));
				} else if (nextSpacePos != curSpacePos && nextSpacePos == -1) { //This is the last word.
					inputArray.add(i, userInput.substring(curSpacePos + 1));
				} else if (nextSpacePos == curSpacePos && curSpacePos != -1) {
					inputArray.add(i, userInput.substring(curSpacePos + 1));
				} else if (curSpacePos == -1) { //This should not occur; this is to catch unexpected errors.
					System.out.println("An unexpected error has occured in the stringToArrayList method's for loop in CmdFileBrowser.java. Please contact the author of this program for assistance.\nError: curSpacePos == -1. This error was successfully caught and recovered from.");
					inputArray.add(i, userInput.substring(0));
				} else {
					System.out.println("An unexpected error has occured in the stringToArrayList method's for loop in CmdFileBrowser.java. Please contact the author of this program for assistance.\nError: else statement reached. This error was successfully caught.");
				}
			}
		} else if (numOfWords == 1 && numOfSpaces == 0) {
			inputArray.add(userInput);
		} else {
			System.out.println("An unexpected error has occured in the stringToArrayList method. Please contact the author of this program for assistance.\nError: The else statement was reached, meaning there is an invalid number of words or spaces. numOfWords=" + numOfWords + "numOfSpaces=" + numOfSpaces);
		}
		return inputArray;
	}
	
	private static ArrayList<String> old2stringToArrayList(String userInput) {
		ArrayList<String> inputArray = new ArrayList<String>();
		int numOfSpaces = 0, numOfWords = 0, curSpacePos = 0, nextSpacePos = 0;
		
		inputArray.clear();
		curSpacePos = 0;
		nextSpacePos = 0;
		numOfSpaces = 0;
		numOfWords = 0;
		ArrayList<Integer> spacesIndex = new ArrayList<Integer>();
		spacesIndex.add(0);
		
		for (int i = 0; i < userInput.length(); i++) {
			if (userInput.charAt(i) == ' ') {
				numOfSpaces++;
				spacesIndex.add(i);
			}
		}
		spacesIndex.add(-1); //Added for if-statements at the end of the for-loop below.
		numOfWords = numOfSpaces + 1;
		sizeOfArray = numOfWords;
		if (numOfWords > 1 && numOfSpaces >= 1) {
			for (int i = 0; i < numOfWords; i++) {
				curSpacePos = spacesIndex.get(i);
				nextSpacePos = spacesIndex.get(i + 1);
				if (i == 0 && curSpacePos >= 0) {
					inputArray.add(i, userInput.substring(0, nextSpacePos));
				} else if (nextSpacePos >= curSpacePos) { //There is another word to add to the ArrayList.
					inputArray.add(i, userInput.substring(curSpacePos + 1, nextSpacePos));
				} else if (nextSpacePos != curSpacePos && nextSpacePos == -1) { //This is the last word.
					inputArray.add(i, userInput.substring(curSpacePos + 1));
				} else if (nextSpacePos == curSpacePos && curSpacePos != -1) {
					inputArray.add(i, userInput.substring(curSpacePos + 1));
				} else if (curSpacePos == -1) { //This should not occur; this is to catch unexpected errors.
					System.out.println("An unexpected error has occured in the stringToArrayList method's for loop in CmdFileBrowser.java. Please contact the author of this program for assistance.\nError: curSpacePos == -1. This error was successfully caught and recovered from.");
					inputArray.add(i, userInput.substring(0));
				} else {
					System.out.println("An unexpected error has occured in the stringToArrayList method's for loop in CmdFileBrowser.java. Please contact the author of this program for assistance.\nError: else statement reached. This error was successfully caught.");
				}
			}
		} else if (numOfWords == 1 && numOfSpaces == 0) {
			inputArray.add(userInput);
		} else {
			System.out.println("An unexpected error has occured in the stringToArrayList method. Please contact the author of this program for assistance.\nError: The else statement was reached, meaning there is an invalid number of words or spaces. numOfWords=" + numOfWords + "numOfSpaces=" + numOfSpaces);
		}
		return inputArray;
	}
	
	private static boolean inputYesOrNo(String s) {
		if (s.equalsIgnoreCase("yes") || s.equalsIgnoreCase("sure") || s.equalsIgnoreCase("ok") || s.equalsIgnoreCase("true") || s.equalsIgnoreCase("y")) {
			return true;
		} else {
			return false;
		}
	}
	
	private static boolean isCommand(String s) {
		String[] possibleCommands = {"close", "exit", "cd", "cp", "copy", "help", "ls", "dir", "mv", "ren", "run", "exec", "execute", "start"};
		for (int i = 0; i < possibleCommands.length; i++) {
			if (s.equalsIgnoreCase(possibleCommands[i])) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * <p>Takes a given directory and returns the parent directory.</p>
	 * <p>Precondition: dirToUse is a valid directory and does not end with "..".</p>
	 * @param dirToUse String
	 * @return Path of the parent directory.
	 */
	private static Path getParent(String dirToUse) {
		File dirForParent = new File(dirToUse);
		Path newDir = null;
		
		if (dirForParent.getParent() != null) {
			newDir = getPath(dirForParent.getParent());
		} else {
			System.out.println("Error: No parent directory found for the path " + dirToUse);
		}
		return newDir;
	}
	
	private static Path getPath(File f) {
		return f.toPath();
	}
	
	private static Path getPath(String s) {
		return Paths.get(s);
	}
	
	private static String parsePath(File f) {
		return parsePath(f.toString());
	}
	
	private static String parsePath(Path p) {
		return parsePath(p.toString());
	}
	
	private static String parsePath(String s) {
		//Basic tests:
		if (s.equalsIgnoreCase(".")) {
			return curDirStr;
		} else if (s.equalsIgnoreCase("..")) {
			try {
				s = getParent(curDirStr).toString();
			} catch (NullPointerException ex) {
				s = curDirStr;
			}
			return s;
		}
		
		//OS Specific Tests:
		if (osType.equalsIgnoreCase("Win32")) {
			//Changes drive letter to upper case, if present.
			if (Character.isLowerCase(s.charAt(0))) {
				s = Character.toString(Character.toUpperCase(s.charAt(0))) + s.substring(1);
			}
			if (s.substring(s.length() - 2).equalsIgnoreCase(fileSeparator + ".")) { //EX: C:\someDir\.
				s = s.substring(0, s.length() - 1);
			} else if (s.length() > 1 && s.substring(0, 2).equalsIgnoreCase("." + fileSeparator)) { //EX: .\someDir\someDir2
				if (curDirStr.endsWith(fileSeparator)) {
					s = curDirStr + s.substring(2);
				} else {
					s = curDirStr + s.substring(1);
				}
			} else if (s.startsWith(".") && !(s.startsWith(".."))) { //EX: .\someDir\someDir2
				if (curDirStr.endsWith(fileSeparator)) {
					s = curDirStr + s.substring(2);
				} else {
					s = curDirStr + s.substring(1);
				}
			} else if (s.startsWith("..")) { //EX: ..\someDir\someDir2
				try {
					s = getParent(curDirStr).toString() + s.substring(2);
				} catch (NullPointerException ex) {
					s = null;
				}
			}
			
			if (s.contains("." + fileSeparator)) { //EX: C:\someDir\.\someDir2
				int separatorPos = s.indexOf("." + fileSeparator);
				int separatorLength = ("." + fileSeparator).length();
				s = s.substring(0, separatorPos) + s.substring(separatorPos + separatorLength);
			} else if (s.length() > 2 && s.substring(s.length() - 3).equalsIgnoreCase(fileSeparator + "..")) {
				try {
					s = getParent(s.substring(0, s.length() - 2)).toString();
				} catch (NullPointerException ex) { //Catch for non-existent directories.
					s = null;
				}
			}
			
			if (s.endsWith(fileSeparator) && s.length() > 3 && s.charAt(1) == ':') {
				s = s.substring(0, s.length() - 1);
				/* Removes the \ from the end. Cannot have length less than 4 because "H::" (2
				 * chars + ":") makes no sense, while "H:\:" (3 chars + ":") actually seems
				 * logical for the line "Directory of [directory]:".
				 * The check for ":" comes at char 1 because that is only allowed in drive letters
				 * (you cannot have a : in a file or folder name), and any ":" in the drive letter
				 * will come at index 1.
				 */
			} else if (s.length() == 2 && Character.isLetter(s.charAt(0)) && s.charAt(1) == ':') {
				s += fileSeparator;
				/* This adds a / to the end, if one does not already exist, but only for drive
				 * letters (i.e. "C:" becomes "C:\"), because this is how the dir command works
				 * in command prompt, and just looks more correct in general.
				 */
			}
			
			if (Character.isLowerCase(s.charAt(0)) && s.length() > 1 && s.charAt(1) == ':') {
				s = Character.toString(Character.toUpperCase(s.charAt(0))) + s.substring(1);
				/* This is purely cosmetic.
				 * This is for the "     Directory: [directory here]."
				 * I want the drive name to be upper case to match the
				 * behavior of Command Prompt, PowerShell, etc.
				 * On Linux, this will have no effect, as isLowerCase()
				 * will return false, so I do not have to worry about
				 * / being the root directory and getting messed up as a result.
				 */
			}
			
		} else if (osType.equalsIgnoreCase("Linux/Unix")) {
			if (s.length() > 1 && s.substring(s.length() - 2).equalsIgnoreCase(fileSeparator + ".")) { //EX: /home/me/.
				s = s.substring(0, s.length() - 1);
			} else if (s.length() > 1 && s.substring(0, 2).equalsIgnoreCase("." + fileSeparator)) { //EX: ./someDir/someDir2
				if (curDirStr.endsWith(fileSeparator)) {
					s = curDirStr + s.substring(2);
				} else {
					s = curDirStr + s.substring(1);
				}
			} else if (s.startsWith(".") && !(s.startsWith(".."))) { //EX: ./someDir/someDir2
				if (curDirStr.endsWith(fileSeparator)) {
					s = curDirStr + s.substring(2);
				} else {
					s = curDirStr + s.substring(1);
				}
			} else if (s.startsWith("..")) { //EX: ../someDir/someDir2
				try {
					s = getParent(curDirStr).toString() + s.substring(2);
				} catch (NullPointerException ex) {
					s = null;
				}
			} else {
				String tmpPath = curDirStr + fileSeparator + s;
				System.out.println(tmpPath);
				if (Files.isDirectory(getPath(tmpPath), LinkOption.NOFOLLOW_LINKS)) {
					s = tmpPath;
				}
			}
			
			if (s.contains("." + fileSeparator) && !(s.contains(".." + fileSeparator))) { //EX: /home/me/./someDir
				int separatorPos = s.indexOf("." + fileSeparator);
				int separatorLength = ("." + fileSeparator).length();
				s = s.substring(0, separatorPos) + s.substring(separatorPos + separatorLength);
			} else if (s.length() > 2 && s.substring(s.length() - 3).equalsIgnoreCase(fileSeparator + "..")) { //EX: /home/me/..
				try {
					s = getParent(s.substring(0, s.length() - 2)).toString();
				} catch (NullPointerException ex) { //Catch for non-existent directories.
					s = "/";
				}
			}
			
			if (s.endsWith(fileSeparator) && s.length() > 1) {
				s = s.substring(0, s.length() - 1);
				/* Removes the / from the end, unless it is in the root directory ("/"), in
				 * which case it does nothing.
				 */
			}
		} else {
			System.out.println("Error: Unknown OS type. Debugging information:\nosType=" + osType + " fileSeparator=" + fileSeparator);
		}
		
		
		//System.out.println("[DEBUG]:\ns=" + s);
		/* This block of code checks the Strings target and
		 * destination for "." (use current directory) or
		 * ".." (use parent directory), and adjusts the
		 * Strings accordingly.
		 */
		
		/*	if (fblDirOrFile(getPath(s)) == -1) {
				if (curDirStr.endsWith(fileSeparator)) {
					s = curDirStr + s;
				} else {
					s = curDirStr + fileSeparator + s;
				}
			}*/
		
		// Calls the function recursively if the conditions are not yet met. Really, I should redo
		// everything and put it into a for loop that iterates over the entire String.
		// At least, test thoroughly.
		if (s.contains(fileSeparator + ".") || s.contains(fileSeparator + "..")) {
			parsePath(s);
		}
		
		
		System.out.println("[DEBUG] s=" + s);
		
		return s;
	}
	
	private static String parsePath2(String s) {
		//Basic tests:
		if (s.equalsIgnoreCase(".")) {
			return curDirStr;
		} else if (s.equalsIgnoreCase("..")) {
			try {
				s = getParent(curDirStr).toString();
			} catch (NullPointerException ex) {
				s = curDirStr;
			}
			return s;
		} else {
			if (s.startsWith(".") && !(s.startsWith(".."))) { // .\someDir or .\someFile
				if (s.length() > 1 && s.substring(1, 2).equals(fileSeparator)) {
					if (curDirStr.substring(s.length() - 2, s.length() - 1).equals(fileSeparator)) { // Ends in "\", e.g. C:\
						s = curDirStr + s.substring(2); // Avoids adding a duplicate \, e.g. C:\\someDir.
					} else {
						s = curDirStr + s.substring(1); // Adds a \ because there is none.
					}
				}
			} else if (s.startsWith("..")) {
				if (s.length() > 2) {
					if (s.substring(2, 3).equals(fileSeparator) && !(curDirStr.endsWith(fileSeparator))) {
						s = getParent(curDirStr) + s.substring(2);
					} else if (s.substring(2, 3).equals(fileSeparator) && curDirStr.endsWith(fileSeparator)) {
						s = getParent(curDirStr) + s.substring(3);
					} // Shouldn't need any other test cases, as there should not be any ..<file/folder> because you type ..\<dirOrFile>, not ..<dirOrFile>.
				}
			}
		}
		
		//OS Specific Tests:
		if(osType.equalsIgnoreCase("Win32")) {
			if (s.length() < 3) { // Not long enough for drive letter
				if (s.length() == 2 && s.substring(1).equals(fileSeparator)) {
					
				}
			}
			
			
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == '.') {
					//if (i )
					
					if (i < s.length() - 1 && s.charAt(i + 1) == '.') { //.. = parent directory.
						
					}
				}
			}
		} else if (osType.equalsIgnoreCase("Linux/Unix")) {
			
		}
		
		
		System.out.println("[DEBUG] s=" + s);
		
		return s;
	}
}
