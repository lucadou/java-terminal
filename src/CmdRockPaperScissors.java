/**
 * David's Java Terminal Rock Paper Scissors Game
 * A CLI application written in Java that plays a game of Rock, Paper,
 * Scissors with the user.
 * 
 * @author David Lucadou
 * Copyright (C) 2015-2016 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.Scanner;

public class CmdRockPaperScissors {
	static Scanner kb = new Scanner(System.in);
	private static int userWins = 0, compWins = 0, totalRounds = 0, userChoiceInt = 0, compChoiceInt = 0;
	private static String userChoice = "", roundResult = "";
	
	public static void cmdRockPaperScissors() {
		boolean userHasQuit = false;
		do {
			userTurn();
			compTurn();
			compareResponses(userChoiceInt, compChoiceInt);
			printResponses();
			printScoreboard();
			
			System.out.print("\nDo you want to keep playing? ");
			String willUserResign = kb.nextLine();
			if(willUserResign.equalsIgnoreCase("yes") || willUserResign.equalsIgnoreCase("sure") || willUserResign.equalsIgnoreCase("ok") || willUserResign.equalsIgnoreCase("true") || willUserResign.equalsIgnoreCase("y")) {
				userHasQuit = false;
			} else if(willUserResign.equalsIgnoreCase("no") || willUserResign.equalsIgnoreCase("nope") || willUserResign.equalsIgnoreCase("quit") || willUserResign.equalsIgnoreCase("resign") || willUserResign.equalsIgnoreCase("false") || willUserResign.equalsIgnoreCase("n")) {
				userHasQuit = true;
			} else {
				System.out.println("\"" + willUserResign + "\" is not a valid answer. I'm assuming you want to continue.");
				userHasQuit = false;
			}
		} while(!userHasQuit);
		System.out.println("The final score is " + userWins + " - " + compWins + ", with a total of " + totalRounds + " rounds played.");
		printWinner();
	}
	
	private static void userTurn() {
		boolean isUserMoveValid = true;
		userChoice = "";
		do {
			System.out.print("Rock, paper, or scissors? ");
			userChoice = kb.nextLine();
			isUserMoveValid = isMoveValid(userChoice);
		} while(!isUserMoveValid);
		userChoiceInt = convertTextToMove(userChoice);
		
		System.out.println("You chose " + userChoice.toLowerCase() + ".");
	}
	
	private static void compTurn() {
		compChoiceInt = (int)(Math.random() * 3.0);
		System.out.println("Computer chooses " + convertMoveToTextLowerCase(compChoiceInt) + ".");
	}
	
	private static void compareResponses(int userChoiceInt, int compChoiceInt) {
		if(userChoiceInt == compChoiceInt) {
			roundResult = "tie";
			totalRounds++;
		} else if(userChoiceInt == 0 && compChoiceInt == 2) { //User: rock, comp: scissors.
			roundResult = "userWin";
			userWins++;
			totalRounds++;
		} else if(userChoiceInt == 1 && compChoiceInt == 0) { //User: paper, comp: rock.
			roundResult = "userwin";
			userWins++;
			totalRounds++;
		} else if(userChoiceInt == 2 && compChoiceInt == 1) { //User: scissors, comp: paper.
			roundResult = "userWin";
			userWins++;
			totalRounds++;
		} else if(compChoiceInt == 0 && userChoiceInt == 2) { //Comp: rock, user: scissors.
			roundResult = "compWin";
			compWins++;
			totalRounds++;
		} else if(compChoiceInt == 1 && userChoiceInt == 0) { //Comp: paper, user: rock.
			roundResult = "compWin";
			compWins++;
			totalRounds++;
		} else if(compChoiceInt == 2 && userChoiceInt == 1) { //Comp: scissors, user: paper.
			roundResult = "compWin";
			compWins++;
			totalRounds++;
		} else {
			roundResult = "unknown";
			totalRounds++;
			System.out.println("An unexpected error in the method compareResponses() has occured. Please contact the author of this program for assistance.");
		}
	}
	
	private static void printResponses() {
		if(roundResult.equalsIgnoreCase("userWin")) {
			System.out.println(convertMoveToText(userChoiceInt) + " beats " + convertMoveToText(compChoiceInt) + ". You win!");
		} else if(roundResult.equalsIgnoreCase("compWin")) {
			System.out.println(convertMoveToText(compChoiceInt) + " beats " + convertMoveToText(userChoiceInt) + ". You lose.");
		} else if(roundResult.equalsIgnoreCase("tie")) {
			System.out.println("It's a tie!");
		} else if(roundResult.equalsIgnoreCase("unknown")) {
			System.out.println("The method compareResponses() has come across an unexpected error. Please contact the author of this program for assistance.");
		} else {
			System.out.println("An unexpected error has occured in the method printResponses(). Please contact the author of this program for assistance.");
		}
	}
	
	private static void printScoreboard() {
		System.out.println("The score is currently " + userWins + " - " + compWins + ".");
	}
	
	private static void printWinner() {
		if(userWins > compWins) {
			System.out.println("You win!");
		} else if(compWins > userWins) {
			System.out.println("You lost.");
		} else if(userWins == compWins) {
			System.out.println("It's a tie!");
		} else {
			System.out.println("Error in method printWinner(): Invalid input. Contact the author of this program for assistance.");
		}
	}
	
	private static int convertTextToMove(String move) {
		int moveInInts = 0;
		if(move.equalsIgnoreCase("rock")) {
			moveInInts = 0;
		} else if(move.equalsIgnoreCase("paper")) {
			moveInInts = 1;
		} else if(move.equalsIgnoreCase("scissors")) {
			moveInInts = 2;
		} else {
			System.out.println("Error in method convertTextToMove(): invalid input.");
		}
		return moveInInts;
	}
	
	private static String convertMoveToText(int move) {
		String moveInWords = "";
		if(move == 0) {
			moveInWords = "Rock";
		} else if(move == 1) {
			moveInWords = "Paper";
		} else if(move == 2) {
			moveInWords = "Scissors";
		} else {
			System.out.println("Error in method convertMoveToText(): invalid input.");
		}
		return moveInWords;
	}
	
	private static String convertMoveToTextLowerCase(int move) {
		String moveInWords = "";
		if(move == 0) {
			moveInWords = "rock";
		} else if(move == 1) {
			moveInWords = "paper";
		} else if(move == 2) {
			moveInWords = "scissors";
		} else {
			System.out.println("Error in method convertMoveToTextLowerCase(): invalid input.");
		}
		return moveInWords;
	}
	
	private static boolean isMoveValid(String move) {
		if(move.equalsIgnoreCase("rock")) {
			return true;
		} else if(move.equalsIgnoreCase("paper")) {
			return true;
		} else if(move.equalsIgnoreCase("scissors")) {
			return true;
		} else {
			return false;
		}
	}
}