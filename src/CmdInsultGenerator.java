/**
 * Anthony's Java Terminal Insult Generator
 * A CLI application written in Java that insults the user.
 * Warning: This program is known to cause terminal cancer for those in
 * good conscience.
 * 
 * @author Anthony DeCenzo
 * Copyright (C) 2015-2016 Anthony DeCenzo
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

public class CmdInsultGenerator {
	private static String[] insult = new String[19];
	
	public static void cmdInsultGenerator() {
		System.out.print("You are such a ");
		getFirst();
		getSecond();
		getThird();
	}
	
	public static void getFirst() {
		insult[0] = "lazy ";
		insult[1] = "stupid ";
		insult[2] = "insecure ";
		insult[3] = "idiotic ";
		insult[4] = "slimy ";
		insult[5] = "slutty ";
		insult[6] = "smelly ";
		insult[7] = "pompous ";
		insult[8] = "communist ";
		insult[9] = "dicknose ";
		insult[10] = "pie-Eating ";
		insult[11] = "racist ";
		insult[12] = "elitist ";
		insult[13] = "white trash ";
		insult[14] = "drug-loving ";
		insult[15] = "butterface ";
		insult[16] = "tone deaf ";
		insult[17] = "ugly ";
		insult[18] = "creepy ";
		
		int random = (int) (Math.random() * insult.length - 1);

		System.out.print(insult[random]);
	}
	
	public static void getSecond() {
		insult[0] = "douche ";
		insult[1] = "ass ";
		insult[2] = "turd ";
		insult[3] = "rectum ";
		insult[4] = "butt ";
		insult[5] = "cock ";
		insult[6] = "shit ";
		insult[7] = "crotch ";
		insult[8] = "bitch ";
		insult[9] = "turd ";
		insult[10] = "prick ";
		insult[11] = "slut ";
		insult[12] = "taint ";
		insult[13] = "fuck ";
		insult[14] = "dick ";
		insult[15] = "boner ";
		insult[16] = "shart ";
		insult[17] = "nut ";
		insult[18] = "sphincter ";
		
		int random = (int) (Math.random() * insult.length - 1);

		System.out.print(insult[random]);
	}
	
	public static void getThird() {
		insult[0] = "pilot!";
		insult[1] = "canoe!";
		insult[2] = "captain!";
		insult[3] = "pirate!";
		insult[4] = "hammer!";
		insult[5] = "knob!";
		insult[6] = "box!";
		insult[7] = "jockey!";
		insult[8] = "nazi!";
		insult[9] = "waffle!";
		insult[10] = "goblin!";
		insult[11] = "blossum!";
		insult[12] = "biscuit!";
		insult[13] = "clown!";
		insult[14] = "socket!";
		insult[15] = "monster!";
		insult[16] = "hound!";
		insult[17] = "dragon!";
		insult[18] = "balloon!";
		
		int random = (int) (Math.random() * insult.length - 1);

		System.out.print(insult[random] + "\n");
	}
}

