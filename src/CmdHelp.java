/**
 * David's Java Terminal Help Command
 * A CLI application written in Java that displays help menus for
 * various applications when called.
 * 
 * @author David Lucadou
 * Copyright (C) 2015-2016 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.util.ArrayList;

public class CmdHelp {
	private static String cmdForHelp;
	
	public static void cmdHelp(ArrayList<String> arrayCmd) {
		if(arrayCmd.size() > 1) {
			cmdForHelp = arrayCmd.get(1);
		} else {
			cmdForHelp = "noArgs";
		}
		
		if(cmdForHelp.equalsIgnoreCase("noArgs")) {
			help();
		} else if(cmdForHelp.equalsIgnoreCase("close")) {
			helpClose();
		} else if(cmdForHelp.equalsIgnoreCase("credits")) {
			helpCredits();
		} else if(cmdForHelp.equalsIgnoreCase("exit")) {
			helpExit();
		} else if(cmdForHelp.equalsIgnoreCase("GuessTheInt")) {
			helpGuessTheInt();
		} else if(cmdForHelp.equalsIgnoreCase("help")) {
			helpHelp();
		} else if(cmdForHelp.equalsIgnoreCase("InsultGenerator")) {
			helpInsultGenerator();
		} else if(cmdForHelp.equalsIgnoreCase("JohnCena")) {
			helpJohnCena();
		} else if(cmdForHelp.equalsIgnoreCase("LennyFace")) {
			helpLennyFace();
		} else if(cmdForHelp.equalsIgnoreCase("RockPaperScissors")) {
			helpRockPaperScissors();
		} else if(cmdForHelp.equalsIgnoreCase("TicTacToe")) {
			helpTicTacToe();
		} else {
			helpInvalid();
		}
	}
	
	public static void help() {
		System.out.println("To get more detail on a command, type \"help (command)\".");
		System.out.println("Usage: help [command]");
		System.out.println("Here is a list of commands currently available:");
		System.out.println("close			Exits this terminal.");
		System.out.println("credits			Lists credits for the specified command.");
		System.out.println("exit			Exits this terminal.");
		System.out.println("guessTheInt		Plays a game of guess the integer (number) with the user.");
		System.out.println("help			Brings up this menu.");
		System.out.println("insultGenerator		Roasts the user who types this command.");
		System.out.println("johnCena		Answers the age-old question, who is Champ?");
		System.out.println("lennyFace		Prints beautiful ASCII art.");
		System.out.println("rockPaperScissors	Plays a game of Rock, Paper, Scissors with the user.");
		System.out.println("ticTacToe		Plays a game of Tic-Tac-Toe with the user.");
	}
	
	private static void helpClose() {
		System.out.println("Exits the Java terminal.");
		System.out.println("Usage: close");
		System.out.println("No options available.");
		System.out.println("See also: exit");
	}
	
	private static void helpCredits() {
		System.out.println("Prints really helpful resources I used in the making of the specified command.");
		System.out.println("Usage: credits [command]");
	}
	
	private static void helpExit() {
		System.out.println("Exits the Java terminal.");
		System.out.println("Usage: exit");
		System.out.println("No options available.");
		System.out.println("See also: close");
	}
	
	private static void helpGuessTheInt() {
		System.out.println("Plays a game of guess the integer (number) with the user.");
		System.out.println("Usage: guesstheint");
		System.out.println("No options available.");
	}
	
	private static void helpHelp() {
		System.out.println("Nice try.");
		System.out.println("Usage: help [command]");
		System.out.println("No options available.");
	}
	
	private static void helpInsultGenerator() {
		System.out.println("Roasts the user who types this command.");
		System.out.println("Usage: insultgenerator");
		System.out.println("No options available.");
	}
	
	private static void helpJohnCena() {
		System.out.println("Prints out a tribute to the star of /r/UnexpectedCena.");
		System.out.println("Usage: johncena [-a] [-c] [-n] [-p]");
		System.out.println("Options:");
		System.out.println("-a			Tells you who is Champ, prints His name, and paints Him with ASCII art. Equivalent to using -c, -n, and -p.");
		System.out.println("--all		See -a.");
		System.out.println("-c 			Tells you just who is Champ.");
		System.out.println("--champ		See -c");
		System.out.println("-n			Prints out John Cena's name in HUGE ASCII art text.");
		System.out.println("--name		See -n");
		System.out.println("-p			Prints out just ASCII art of John Cena.");
		System.out.println("--portrait	See -p");
	}
	
	private static void helpLennyFace() {
		System.out.println("Prints out le lenny face. xD :^)");
		System.out.println("Usage: lennyface");
		System.out.println("No options available.");
	}
	
	private static void helpRockPaperScissors() {
		System.out.println("Plays a game of Rock, Paper, Scissors with the user.");
		System.out.println("Usage: rockpaperscissors");
		System.out.println("No options available.");
	}
	
	private static void helpTicTacToe() {
		System.out.println("Plays a game of Tic-Tac-Toe with the user.");
		System.out.println("Usage: tictactoe");
		System.out.println("No options available.");
	}
	
	private static void helpInvalid() {
		System.out.println("help: Invalid option.");
		System.out.println("Usage: help [command]");
	}
}
