/**
 * David's Java Terminal Tic-Tac-Toe Command
 * A CLI application written in Java that interacts with the user in a
 * game of Tic-Tac-Toe.
 * 
 * @author David Lucadou
 * Copyright (C) 2015 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 */

import java.util.Scanner;

/*
 * Stuff to do:
 * 1. Rewrite drawTable with unicode chars: http://www.utf8-chartable.de/unicode-utf8-table.pl Box Drawing.
 * OR
 * 1. Try extended ASCII class from: http://stackoverflow.com/questions/22273046/how-to-print-the-extended-ascii-code-in-java-from-integer-value
 * 
 * 2. Add this to credits: http://stackoverflow.com/questions/5585779/converting-string-to-int-in-java (need credits cmd).
 * 
 */

public class CmdTicTacToe {
	private static Scanner kb = new Scanner(System.in);
	private static char[][] gV = new char[3][3]; // gV = Grid Values.
	private static char userChar, compChar;
	private static boolean isSpaceValid, isSpaceTaken, isCoinValid, doesComputerHaveFirstMove, gameOver, userWon, newGame;
	private static double headsOrTails;
	private static int gVrow, gVcol;
	private static String userChosenChar, userCoinFlip, userSpace, compSpace, gameStatus;
	
	public static void cmdTicTacToe() {
		System.out.print("Do you want to be X's or O's? ");
		userChosenChar = kb.nextLine();
		if(userChosenChar.equalsIgnoreCase("x")) {
			userChar = 'X';
			compChar = 'O';
			System.out.println("\nOK, I will be " + compChar + " then.");
			startGame();
		} else if(userChosenChar.equalsIgnoreCase("o")) {
			userChar = 'O';
			compChar = 'X';
			System.out.println("\nOK, I will be " + compChar + " then.");
			startGame();
		} else {
			System.out.println("Please enter X or O next time. Exiting...");
		}
	}
	
	private static void startGame() {
		System.out.print("Heads or tails? ");
		userCoinFlip = kb.nextLine();
		headsOrTails(userCoinFlip);
		
		if(isCoinValid) {
			if(!doesComputerHaveFirstMove) {
				while(!gameOver) {
					tttUserTurn();
					tttCompTurn();
					isGameOver();
				}
				if(!newGame) {
					System.out.println("Thanks for playing!");
				} else {
					tttStartNewGame();
				}
			} else {
				while(!gameOver) {
					tttCompTurn();
					tttUserTurn();
					isGameOver();
				}
				if(!newGame)
				{
					System.out.println("Thanks for playing!");
				} else {
					tttStartNewGame();
				}
			}
		}
	}
	
	private static void tttStartNewGame() {
		CmdTicTacToe.startGame();
	}

	private static void tttUserTurn() {
		System.out.println("\nThis is the board currently:");
		System.out.println(drawBoard());
		System.out.print("Enter the space you want to mark. For example, row A column 1 would be A1 (no spaces). ");
		userSpace = kb.nextLine();
		if(isChosenSpaceValid(userSpace) && !isChosenSpaceTaken(userSpace)) {
			gV[gVrow][gVcol] = userChar;
		} else {
			if(!isChosenSpaceValid(userSpace)) {
				System.out.println("Please enter a valid space.");
				tttUserTurn();
			} else if(isChosenSpaceTaken(userSpace)) {
				System.out.println("That space was already taken, choose a free space.");
				tttUserTurn();
			}
		}
	}
	
	private static void tttCompTurn() {
		char row;
		
		int compRow = (int)(Math.random() * 3.0);
		int compColumn = (int)(Math.random() * 3.0);
		
		switch(compRow) {
		case 0:
			row = 'a';
			break;
		case 1:
			row = 'b';
			break;
		case 2:
			row = 'c';
			break;
		default:
			row = 'a';
			break;
		}
		compSpace = Character.toString(row) + Integer.toString(compColumn);
		if(isChosenSpaceValid(compSpace) && !isChosenSpaceTaken(compSpace)) {
			gV[gVrow][gVcol] = compChar;
		} else {
			tttCompTurn();
		}
	}
	
	private static void getGridPos(char row, int column) {
		row = Character.toUpperCase(row);
		
		switch (row) {
			case 'A':
				gVrow = 0;
				gVcol = column - 1;
				break;
			case 'B':
				gVrow = 1;
				gVcol = column - 1;
				break;
			case 'C':
				gVrow = 2;
				gVcol = column - 1;
				break;
		}
	}
	
	/**
	 * <p>Checks to see if there are any winning combinations.</p>
	 * <p>If there are, it sets {@code gameStatus} to the appropriate value. However, if any of the values are null ({@code '\0'}), it will ignore those.</p>
	 * <p>If the board is full but there are no winning combinations, it is a tie game.</p>
	 * 
	 * @return String gameStatus - userWin, compWin, active, or stall.
	 */
	public static String getGameStatus() {
		if(gV[0][0] == gV[0][1] && gV[0][1] == gV[0][2] && gV[0][0] != '\0') { // Top row.
			if(gV[0][0] == userChar) {
				gameStatus = "userWin";
				userWon = true;
			} else if(gV[0][0] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[1][0] == gV[1][1] && gV[1][1] == gV[1][2] && gV[1][0] != '\0') { // Middle row.
			if(gV[1][0] == userChar) {
				gameStatus = "userWin";
			} else if(gV[1][0] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[2][0] == gV[2][1] && gV[2][1] == gV[2][2] && gV[2][0] != '\0') { // Bottom row.
			if(gV[2][0] == userChar) {
				gameStatus = "userWin";
			} else if(gV[2][0] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[0][0] == gV[1][0] && gV[1][0] == gV[2][0] && gV[0][0] != '\0') { // Left column.
			if(gV[0][0] == userChar) {
				gameStatus = "userWin";
			} else if(gV[0][0] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[0][1] == gV[1][1] && gV[1][1] == gV[2][1] && gV[0][1] != '\0') { // Middle column.
			if(gV[0][1] == userChar) {
				gameStatus = "userWin";
			} else if(gV[0][1] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[0][2] == gV[1][2] && gV[1][2] == gV[2][2] && gV[0][2] != '\0') { // Right column.
			if(gV[0][2] == userChar) {
				gameStatus = "userWin";
			} else if(gV[0][2] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[0][0] == gV[1][1] && gV[1][1] == gV[2][2] && gV[0][0] != '\0') { // Diagonal, top of left column to bottom of right column.
			if(gV[0][0] == userChar) {
				gameStatus = "userWin";
			} else if(gV[0][0] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[0][2] == gV[1][1] && gV[1][1] == gV[2][0] && gV[0][2] != '\0') { // Diagonal, bottom of left column to top of left column.
			if(gV[0][2] == userChar) {
				gameStatus = "userWin";
			} else if(gV[0][2] == compChar) {
				gameStatus = "compWin";
			}
		} else if(gV[0][0] != '\0' && gV[0][1] != '\0' && gV[0][2] != '\0' && gV[1][0] != '\0' && gV[1][1] != '\0' && gV[1][2] != '\0' && gV[2][0] != '\0' && gV[2][1] != '\0' && gV[2][2] != '\0') {
			gameStatus = "stall"; // If no spaces are empty, and there is no winner, it must be a stall.
		} else {
			gameStatus = "active";
		}
		
		return gameStatus; //seems to slip setting gameStatus to "active", investigate.
	}
	
	/**
	 * <p>The {@code isChosenSpaceTaken} method tells you if the specified point is marked (that is, if the position in the array has a value of X or O already).</p>
	 * <p>{@code gV[gVpos] == '\0'} is used because it is the {@code null} value for type {@code char}.</p>
	 * 
	 * @param userSpace - The specified row (letter) and column (number) combination.
	 * @return boolean isSpaceTaken - This returns whether or not a space has been marked.
	 */
	private static boolean isChosenSpaceTaken(String userSpace) {
		char row = userSpace.charAt(0);
		int column = Integer.valueOf(userSpace.substring(1));
		//int column = userSpace.charAt(1); //THIS is where the problem is. NEED to FIX!
		
		getGridPos(row, column);
		if(gV[gVrow][gVcol] == '\0') {
			isSpaceTaken = false;
		} else {
			isSpaceTaken = true;
		}
		return isSpaceTaken;
	}
	
	/**
	 * <p>The {@code isChosenSpaceValid} method tells you if the specified space is a valid move based on whether or not the space is a valid point on the grid (does not depend on capitalization).</p>
	 * <p>{@code row} is checked to see if it is a, b, or c (upper or lower case).<br>
	 * {@code column} is checked to see if it is 1, 2, or 3.</p>
	 * <p>If both are {@code true}, then {@code isSpaceValid} is true.</p>
	 * @param userSpace - The specified row (letter) and column (number) combination.
	 * @return boolean isSpaceValid - This returns whether or not the space is a valid move.
	 */
	private static boolean isChosenSpaceValid(String userSpace) {
		char row = userSpace.charAt(0);
		int column = Integer.valueOf(userSpace.substring(1));
				
		if((row == 'a' || row == 'A' || row == 'b' || row == 'B' || row == 'c' || row == 'C') && (column == 1 || column == 2 || column == 3)) {
			isSpaceValid = true;
		} else {
			isSpaceValid = false;
		}
		return isSpaceValid;
	}
	
	/**
	 * <p>This method draws out the board in beautiful UTF-8 art.</p>
	 * @return String - the board.
	 */
	public static String drawBoard() {
		return "  ╔═══╦═══╦═══╗\nA ║ " + gV[0][0] + " ║ " + gV[0][1] + " ║ " + gV[0][2] + " ║\n  ╠═══╬═══╬═══╣\nB ║ " + gV[1][0] + " ║ " + gV[1][1] + " ║ " + gV[1][2] + " ║\n  ╠═══╬═══╬═══╣\nC ║ " + gV[2][0] + " ║ " + gV[2][1] + " ║ " + gV[2][2] + " ║\n  ╚═══╩═══╩═══╝\n    1   2   3";
		/*
		 * Starting image:
		 *   ╔═══╦═══╦═══╗\n
		 * A ║   ║   ║   ║\n
		 *   ╠═══╬═══╬═══╣\n
		 * B ║   ║   ║   ║\n
		 *   ╠═══╬═══╬═══╣\n
		 * C ║   ║   ║   ║\n
		 *   ╚═══╩═══╩═══╝\n
		 *    1   2   3
		 * I just wrote the ASCII without regard to \n's so I could
		 * design it properly. Then, I replaced "   " (3 spaces, an
		 * empty box), with "" + a1 + "", and fixed them to b1, c2,
		 * etc.  Then, I put them all on one line, so the \n's would
		 * work and it would use 1 return statement. That is how I
		 * wrote this code.
		 * 
		 */
	}
	
	/**
	 * <p>This method determines if the game is over by calling the function {@code getGameStatus()} and prompting the user to continue or quit based on the result (win, lose, or tie), or continues if the game is not over.</p>
	 * <p>Acceptable answers for continuing to play:<br>
	 * Yes, sure, ok, okay, true, y.<br>
	 * Acceptable answers for not continuing to play:<br>
	 * No, nope, quit, resign, false, n.</p>
	 * <p>Note: Neither of these are case sensitive.</p>
	 * 
	 * @return boolean gameOver - Returns whether or not the game is over (that is, if there are any moves left to make).
	 */
	public static boolean isGameOver() {
		getGameStatus();
		
		boolean isAnswerAcceptable = true;
		if(gameStatus.equalsIgnoreCase("active")) {
			gameOver = false;
		} else if(gameStatus.equalsIgnoreCase("stall")) {
			do {
				System.out.println("Tie game. Would you like to play again?");
				String willUserDoNewGame = kb.next();
				if(willUserDoNewGame.equalsIgnoreCase("yes") || willUserDoNewGame.equalsIgnoreCase("sure") || willUserDoNewGame.equalsIgnoreCase("ok") || willUserDoNewGame.equalsIgnoreCase("okay") || willUserDoNewGame.equalsIgnoreCase("true") || willUserDoNewGame.equalsIgnoreCase("y")) {
					newGame = true;
					isAnswerAcceptable = true;
				} else if(willUserDoNewGame.equalsIgnoreCase("no") || willUserDoNewGame.equalsIgnoreCase("nope") || willUserDoNewGame.equalsIgnoreCase("quit") || willUserDoNewGame.equalsIgnoreCase("resign") || willUserDoNewGame.equalsIgnoreCase("false") || willUserDoNewGame.equalsIgnoreCase("n")) {
					newGame = false;
					isAnswerAcceptable = true;
				} else {
					isAnswerAcceptable = false;
				}
			} while(!isAnswerAcceptable);
			gameOver = true;
		} else if(gameStatus.equalsIgnoreCase("userWin")) {
			do {
				System.out.println("Congratulations, you won! Would you like to play again?");
				String willUserDoNewGame = kb.next();
				if(willUserDoNewGame.equalsIgnoreCase("yes") || willUserDoNewGame.equalsIgnoreCase("sure") || willUserDoNewGame.equalsIgnoreCase("ok") || willUserDoNewGame.equalsIgnoreCase("okay") || willUserDoNewGame.equalsIgnoreCase("true") || willUserDoNewGame.equalsIgnoreCase("y")) {
					newGame = true;
					isAnswerAcceptable = true;
				} else if(willUserDoNewGame.equalsIgnoreCase("no") || willUserDoNewGame.equalsIgnoreCase("nope") || willUserDoNewGame.equalsIgnoreCase("quit") || willUserDoNewGame.equalsIgnoreCase("resign") || willUserDoNewGame.equalsIgnoreCase("false") || willUserDoNewGame.equalsIgnoreCase("n")) {
					newGame = false;
					isAnswerAcceptable = true;
				} else {
					isAnswerAcceptable = false;
				}
			} while(!isAnswerAcceptable);
			gameOver = true;
		} else if(gameStatus.equalsIgnoreCase("compWin")) {
			do {
				System.out.println("You lost. Would you like to play again?" );
				String willUserDoNewGame = kb.next();
				if(willUserDoNewGame.equalsIgnoreCase("yes") || willUserDoNewGame.equalsIgnoreCase("sure") || willUserDoNewGame.equalsIgnoreCase("ok") || willUserDoNewGame.equalsIgnoreCase("okay") || willUserDoNewGame.equalsIgnoreCase("true") || willUserDoNewGame.equalsIgnoreCase("y")) {
					newGame = true;
					isAnswerAcceptable = true;
				} else if(willUserDoNewGame.equalsIgnoreCase("no") || willUserDoNewGame.equalsIgnoreCase("nope") || willUserDoNewGame.equalsIgnoreCase("quit") || willUserDoNewGame.equalsIgnoreCase("resign") || willUserDoNewGame.equalsIgnoreCase("false") || willUserDoNewGame.equalsIgnoreCase("n")) {
					newGame = false;
					isAnswerAcceptable = true;
				} else {
					isAnswerAcceptable = false;
				}
			} while(!isAnswerAcceptable);
			gameOver = true;
		}
		return gameOver;
	}
	
	/**
	 * The {@headsOrTails} method takes the user's pick of {@code heads} or {@code tails} and generates
	 * a random number using {@code Math.random()}.
	 * <br>
	 * A value of {@code heads} is returned if the random number is greater than or equal to 0.5.
	 * <br>
	 * A value of {@code tails} is returned if the random number is less than 0.5
	 * <br>
	 * I chose less than for tails because {@code Math.random()} goes from 0 up to 1, but does not include 1, so that is my way of evening out the slight unfairness.
	 * @param userCoinFlip - Heads or tails, picked by the user.
	 * @return boolean doesComputerhaveFirstMove - Returns whether or not the computer has the first move.
	 */
	private static boolean headsOrTails(String userCoinFlip) {
		headsOrTails = Math.random();
		if(headsOrTails >= 0.5) {
			if(userCoinFlip.equalsIgnoreCase("heads") || userCoinFlip.equalsIgnoreCase("head")) {
				System.out.println("It's heads! You get the first move!");
				isCoinValid = true;
				doesComputerHaveFirstMove = false;
			} else if(userCoinFlip.equalsIgnoreCase("tails") || userCoinFlip.equalsIgnoreCase("tail")) {
				System.out.println("It's heads, the computer will get the first move.");
				isCoinValid = true;
				doesComputerHaveFirstMove = true;
			} else {
				System.out.println("You did not choose heads or tails, please enter \"heads\" or \"tails\" next time. Exiting...");
				isCoinValid = false;
			}
		} else if(headsOrTails < 0.5) {			
			if(userCoinFlip.equalsIgnoreCase("tails") || userCoinFlip.equalsIgnoreCase("tail")) {
				System.out.println("It's tails! You get the first move!");
				isCoinValid = true;
				doesComputerHaveFirstMove = false;
			} else if(userCoinFlip.equalsIgnoreCase("heads") || userCoinFlip.equalsIgnoreCase("head")) {
				System.out.println("It's tails, the computer will get the first move.");
				isCoinValid = true;
				doesComputerHaveFirstMove = true;
			} else {
				System.out.println("You did not choose heads or tails, please enter \"heads\" or \"tails\" next time. Exiting...");
				isCoinValid = false;
			}
		}
		return doesComputerHaveFirstMove;
	}
}
