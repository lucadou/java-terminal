/**
 * David's Java Terminal
 * A CLI application written in Java that uses commands like a real
 * terminal to run various mini-programs.
 * 
 * @author David Lucadou
 * Copyright (C) 2015-2016 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class TerminalLaunch {
	public static void main(String[] args) throws IOException {
		//To do:
		//1. Redo CmdFileBrowser using Apache Commons, probably. Fuck Java IO & NIO classes.
		//2. Redo launch and command parser to use Lists, rather than ArrayLists (best practice).
		Scanner kb = new Scanner(System.in);
		
		System.out.println("  __________________________________________________");
		System.out.println("/ Welcome to David & Anthony\'s Java based Terminal! \\");
		System.out.println("|        Enter a command or type \"help\" for a       |");
		System.out.println("\\            list of available commands!            /");
		System.out.println(" \\-------------------------------------------------/");
		System.out.println("         \\   ^__^ ");
		System.out.println("          \\  (oo)\\_______");
		System.out.println("             (__)\\       )\\/\\");
		System.out.println("                 ||----w |");
		System.out.println("                 ||     ||");
		System.out.println("        ");
		System.out.println("");
		
		String cmd = "";
		ArrayList<String> arrayCmd = new ArrayList<String>();
		int numOfWords = 0, numOfSpaces = 0, curSpacePos = 0, nextSpacePos = 0;
		do {
			arrayCmd.clear();
			curSpacePos = 0;
			nextSpacePos = 0;
			numOfSpaces = 0;
			numOfWords = 0;
			
			System.out.print("terminal@java~$ ");
			cmd = kb.nextLine();
			
			for(int i = 0; i < cmd.length(); i++) {
				if(cmd.charAt(i) == ' ') {
					numOfSpaces++;
				}
			}
			numOfWords = numOfSpaces + 1;
			
			for(int i = 0; i < numOfWords; i++) {
				curSpacePos = cmd.indexOf(' ', curSpacePos);
				nextSpacePos = cmd.indexOf(' ', curSpacePos);
				if(i == 0 && curSpacePos >= 0) {
					arrayCmd.add(i, cmd.substring(0, curSpacePos));
				} else if(nextSpacePos != curSpacePos) {
					arrayCmd.add(i, cmd.substring(curSpacePos + 1, nextSpacePos));
				} else if(nextSpacePos == curSpacePos && curSpacePos != -1) {
					arrayCmd.add(i, cmd.substring(curSpacePos + 1));
				} else if(curSpacePos == -1) {
					arrayCmd.add(i, cmd.substring(0));
				} else {
					System.out.println("An unexpected error has occured in the arrayCmd for loop in TerminalLaunch.java. Please contact the author of this program for assistance.");
				}
			}
			
			CommandParser.commandParser(arrayCmd);
		} while(!cmd.equalsIgnoreCase("exit") &&  !cmd.equalsIgnoreCase("close"));
		System.out.println("Goodbye!");
		kb.close();
	}
}
