/**
 * David's Java Terminal Command Parser
 * A CLI application written in Java that parses commands to determine
 * which class to call for a given command.
 * 
 * @author David Lucadou
 * Copyright (C) 2015-2016 David Lucadou
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

import java.io.IOException;
import java.util.ArrayList;

public class CommandParser {
	private static String mainCurCmdName;
	
	public static void commandParser(ArrayList<String> arrayCmd) throws IOException {
		mainCurCmdName = arrayCmd.get(0);
		
		if(mainCurCmdName.equalsIgnoreCase("help")) {
			CmdHelp.cmdHelp(arrayCmd);
		} else if(mainCurCmdName.equalsIgnoreCase("Credits")) {
			CmdCredits.cmdCredits(arrayCmd);
		} else if(mainCurCmdName.equalsIgnoreCase("FileBrowser")) {
			CmdFileBrowser.cmdFileBrowser();
		} else if(mainCurCmdName.equalsIgnoreCase("FizzBuzz")) {
			CmdFizzBuzz.cmdFizzBuzz();
		} else if(mainCurCmdName.equalsIgnoreCase("GuessTheInteger") || mainCurCmdName.equalsIgnoreCase("GuessTheInt")) {
			CmdGuessTheInt.cmdGuessTheInt();
		} else if(mainCurCmdName.equalsIgnoreCase("InsultGenerator")) {
			CmdInsultGenerator.cmdInsultGenerator();
		} else if(mainCurCmdName.equalsIgnoreCase("JohnCena")) {
			CmdJohnCena.cmdJohnCena(arrayCmd);
		} else if(mainCurCmdName.equalsIgnoreCase("LennyFace")) {
			CmdLennyFace.cmdLennyFace();
		} else if(mainCurCmdName.equalsIgnoreCase("RockPaperScissors")) {
			CmdRockPaperScissors.cmdRockPaperScissors();
		} else if(mainCurCmdName.equalsIgnoreCase("TicTacToe")) {
			CmdTicTacToe.cmdTicTacToe();
		} else if(mainCurCmdName.equalsIgnoreCase("exit") || mainCurCmdName.equalsIgnoreCase("close")) {
			// This does nothing, there is no command to forward it to, as TerminalLaunch will
			// automatically exit since cmd == "exit" or "close". But it still displays "Please
			// input a valid command." for some reason, and I'll probably look into that later.
			// For now, though, this is just to prevent the invalid command message from displaying.
		} else if(mainCurCmdName.equalsIgnoreCase("none")) {
			System.out.println("That is not a valid command!");
		} else {
			System.out.println("Please input a valid command.");
		}
	}	
}