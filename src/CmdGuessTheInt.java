/**
 * Anthony's Java Terminal Guess the Integer Game
 * A CLI application written in Java that plays a game of guess that
 * integer (between 1-100).
 * 
 * @author Anthony DeCenzo
 * Copyright (C) 2015-2016 Anthony DeCenzo
 * 
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


import java.util.Random;
import java.util.Scanner;
	 
public class CmdGuessTheInt {
	public static void cmdGuessTheInt() {
		boolean go = true;
		int randomNum = getRandom(1, 100);
		
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		SOP("Enter your guess: ");
		int guess = input.nextInt();
		
		do {
			if(guess == randomNum) {
				SOP("Your guess " + guess + " was correct!");
				go = false;
				break;
			}
			else if(guess < randomNum) {
				SOP("Enter a number that is greater than your previous number");
				SOP("Enter your next guess: ");
				guess = input.nextInt();
				continue;
			}
			else if(guess > randomNum) {
				SOP("Enter a number that is less than your previous number");
				SOP("Enter your next guess: ");
				guess = input.nextInt();
				continue;
			}
	               
		} while(go == true);
	}
  
	public static void SOP(String str) {
		System.out.println(str);
	}
	   
	public static int getRandom(int min, int max) {
		Random rand = new Random();
	       
		int randomNum = rand.nextInt((max - min) + 1) + min;
	       
		return randomNum;
	}
}

